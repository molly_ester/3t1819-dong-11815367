#include "Types.h"
#include "Pokemon.h"
#include "Moves.h"



Types::Types(){
	this->name = "";

}

Types::Types(string name, vector<string> strength, vector<string> weaknesses)
{
	this->name = name;
	this->strength = strength;
	this->weaknesses = weaknesses;
}

Types::Types(string name, vector<string> immunities, vector<string> strength, vector<string> weaknesses)
{
	this->name = name;
	this->immune = immunities;
	this->strength = strength;
	this->weaknesses = weaknesses;
}


//this == the attacking pokemon;
