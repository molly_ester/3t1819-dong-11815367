#include "Trainer.h"
#include "Pokemon.h"
#include "Map.h"
#include "Types.h"
#include "Moves.h"


Trainer::Trainer()
{
}

Trainer::Trainer(string name) {
	this->name = name;
	this->pokeDollars = 5000;
	this->pokeballs[0] = 5;
}

void Trainer::moveHorizontal(int x)
{
	if (x == 1) { 
		this->x++;
		cout << "you moved to the east.\n";
	}
	else {
		this->x--;
		cout << "you moved to the west.\n";
	}
	this->moveHorizontalLocation();
}

void Trainer::moveVertical(int y)
{
	if (y == 1) {
		this->y++;
		cout << "you moved to the north.\n";
	}
	else {
		this->y--;
		cout << "you moved to the south.\n";
	}
	this->moveVerticalLocation();
}

void Trainer::moveHorizontalLocation()
{
	if (this->x < this->location->xL) {
		if (this->location->West != NULL) 
			this->location = this->location->West;
		else {
			this->x++;
			cout << "but, you can't traverse this area" << endl;
		}
	}
	else if (this->x > this->location->xR) {
		if (this->location->East != NULL)
			this->location = this->location->East;
		else {
			cout << "but, you can't traverse this area" << endl;
			this->x--;
		}
	}
	
}

void Trainer::moveVerticalLocation()
{
	if (this->y > this->location->yU) {
		if (this->location->North != NULL) 
			this->location = this->location->North;
		else {
		this->y--;
		cout << "but, you can't traverse this area" << endl;
		}
	}
	else if (this->y < this->location->yD) {
		if (this->location->South != NULL) this->location = this->location->South;
		else {
			cout << "but, you can't traverse this area" << endl;
			this->y++;
		}
	}
}

bool Trainer::capture(Pokemon* pokemon)
{
	int choice;
	cout << "what pokeball would you like to use\n\t[1] Pokeball\n\t[2] Great Ball\n\t[3] Ultra Ball\n";
	do cin >> choice; while (choice > 3 || choice < 1);

	if (this->pokeballs[choice - 1] > 0){
		if (rand() % 100 + 1 <= (choice * 10) + 20) {
			this->pokemon.push_back(pokemon);
			cout << "You successfully captured " << pokemon->name << endl;
			this->pokeballs[choice - 1]--;
			return true;
		}
		else {
			cout << pokemon->name << " got out...\n";
			this->pokeballs[choice - 1]--;
			return false;
		}
	}
	else {
		cout << "you currently have no pokeballs\n";
		return false;
	}
}

void Trainer::buyItems(int price, int choice, int howMany)
{
	cout << "Successfully bought the items...\n";
	this->pokeDollars -= price * howMany;
	this->pokeballs[choice - 1] += howMany;
}

void Trainer::switchPokemons() {
	int choice;
	if (this->pokemon.size() > 1) {
		for (int i = 1; i < this->pokemon.size(); i++) {
			cout << "[" << i << "] " << this->pokemon[i]->name << endl;
		}
		do cin >> choice; while (choice > this->pokemon.size() - 1 && choice < 1);
		swap(this->pokemon[0], this->pokemon[choice]);
	}
	else cout << "you don't have any other pokemon\n";
}

void Trainer::blackedOut() {
	int pokemonfainted = 0;
	for (int i = 0; i < this->pokemon.size(); i++) if (this->pokemon[i]->hp <= 0) pokemonfainted++;
	if (pokemonfainted == pokemon.size()) {
		cout << this->name << " blacked out...";
		for (int i = 0; i < this->pokemon.size(); i++) this->pokemon[i]->hp = this->pokemon[i]->maxHp;
		this->y = 0;
		this->moveVerticalLocation();
		this->x = 0;
		this->moveHorizontalLocation();
	}
}

bool Trainer::chickenOut() {
	cout << "you ran away..." << endl;
	return true;
}

void Trainer::goPokemart() {
	int choice;
	int howMany;
	int price;
	do {
		cout << "Welcome to the pokeMart Would you like to buy some pokeballs?\nYou currently have " << this->pokeDollars << " pokedollars\n";
		cout << "Pokeballs: " << this->pokeballs[0] << "\tGreatballs: " << this->pokeballs[1] << "\tUltraBalls: " << this->pokeballs[2] << endl;
		cout << "\t[1] Pokeballs\n\t[2] Greatballs\n\t[3] Ultraballs\n\t[4] Back\n";
		do cin >> choice; while (choice > 4 || choice < 1);
		if (choice == 1) {
			price = 200;
			cout << "How many pokeballs would u like to buy?\n";
			cin >> howMany;
		}
		else if (choice == 2) {
			price = 600;
			cout << "How many Greatballs would u like to buy?\n";
			cin >> howMany;
		}
		else if (choice == 3) {
			price = 1200;
			cout << "How many Ultraballs would u like to buy?\n";
			cin >> howMany;
		}
		else break;

		if (price * howMany <= this->pokeDollars) this->buyItems(price, choice, howMany);
		else "Insufficient Money...\n";
	} while (choice > 3 || choice < 1);
}

void Trainer::goPokemonCenter() {
	cout << "Welcome to the pokemon center... We will heal up your Pokemon to its full health...\nPlease wait a minute, it will be done soon...\n";
	for (int i = 0; i < this->pokemon.size(); i++) this->pokemon[i]->hp = this->pokemon[i]->maxHp;
	_getch();
	cout << "The pokemon have been healed to their full health.\n";
}

void Trainer::showAllPokemon() {
	for (int i = 0; i < this->pokemon.size(); i++) {
		this->pokemon[i]->showStats();
	}
	_getch();
}

void Trainer::displayPosition() {
	cout << "(" << this->x << ", " << this->y << ") / " << this->location->name << endl;
}

void Trainer::gainPokeDollars(int money) {
	this->pokeDollars += money;
	cout << this->name << " gained " << money << " PokeDollars" << endl;
}