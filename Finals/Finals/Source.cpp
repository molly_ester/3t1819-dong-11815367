#include<iostream>
#include<string>
#include<time.h>
#include<conio.h>

#include"Pokemon.h"
#include"Moves.h"
#include"Trainer.h"
#include"Types.h"
#include"Map.h"

using namespace std;
//these are pokemon types that will be used in the whole game
Types* types(int type) {
	Types* types[10];
	types[0] = new Types("Water", { "Fire", "Water" }, { "Electric", "Grass" });
	types[1] = new Types("Grass", { "Water", "Ground", "Electric" ,"Grass" }, { "Poison", "Flying", "Fire" });
	types[2] = new Types("Electric", { "Electric" }, { "Ground" });
	types[3] = new Types("Psychic", { "Fighting", "Psychic" }, {});
	types[4] = new Types("Poison", { "FIghting", "Poison", "Grass" }, { "Ground", "Psychic" });
	types[5] = new Types("Flying", { "Ground" }, { "Grass", "Fighting" }, { "Electric" });
	types[6] = new Types("Fighting", {}, { "Psychic", "Flying" });
	types[7] = new Types("Normal", {}, { "Fighting" });
	types[8] = new Types("Ground", { "Electric" }, { "Poison" }, { "Water", "Grass" });
	types[9] = new Types("Fire", { "Fire", "Grass" }, { "Water","Ground" });
	return types[type];
}
//these are the locations as well as the initialization of the map in the game
Map* Hoenn() {
	Map* mauville = new Map(-2, 2, 2, -2, false, "Mauville City"); 
	Map* route1 = new Map(3, 5, 2, -2, true, "Route 118"); // east of mauville
	Map* route3 = new Map(2, -2, -3, -5, true, "Route 110"); //south of mauville
	Map* route4 = new Map(2, -2, 5, 3, true, "Route 111"); //north of mauville
	Map* route2 = new Map(-5, -3, 2, -2, true, "Route 117"); //west of mauville

	mauville->East = route1;
	route1->West = mauville;

	mauville->North = route4;
	route4->South = mauville;

	route2->East = mauville;
	mauville->West = route2;

	mauville->South = route3;
	route3->North = mauville;

	return mauville;
}
//these are the pokemons that can appear in the Wild
Pokemon* pokemons() {
	int randomLvl = rand() % 10 + 1;
	int randomPoke = rand() % 5;
	Pokemon* pokemon[5];
	pokemon[0] = new Pokemon("Plusle", randomLvl + 15, randomLvl + 3, randomLvl + 1, randomLvl);
	pokemon[0]->type[0] = types(2);
	pokemon[0]->move = new Moves("Spark", types(2), 65);

	pokemon[1] = new Pokemon("Koffing", randomLvl + 20, randomLvl + 2, randomLvl + 1, randomLvl);
	pokemon[1]->type[0] = types(4);
	pokemon[1]->evolveLvl = 35;
	pokemon[1]->evolve = new Pokemon("Weezing");
	pokemon[1]->evolve->type[0] = types(4);
	pokemon[1]->move = new Moves("Clear Smog", types(4), 50);
	pokemon[1]->evolve->move = new Moves("Sludge Bomb", types(4), 90);

	pokemon[2] = new Pokemon("Spinda", randomLvl + 15, randomLvl + 1, randomLvl + 1, randomLvl);
	pokemon[2]->type[0] = types(7);
	pokemon[2]->move = new Moves("Dizzy Punch", types(7), 70);

	pokemon[3] = new Pokemon("Meditite", randomLvl + 13, randomLvl + 4, randomLvl, randomLvl);
	pokemon[3]->type[0] = types(6);
	pokemon[3]->type[1] = types(3);
	pokemon[3]->evolveLvl = 35;
	pokemon[3]->evolve = new Pokemon("Medicham");
	pokemon[3]->evolve->type[0] = types(6);
	pokemon[3]->evolve->type[1] = types(3);
	pokemon[3]->move = new Moves("Confusion", types(3), 50);
	pokemon[3]->evolve->move = new Moves("High Jump Kick", types(6), 130);

	pokemon[4] = new Pokemon("Tropius", randomLvl + 25, randomLvl + 3, randomLvl + 1, randomLvl);
	pokemon[4]->type[0] = types(1);
	pokemon[4]->type[1] = types(5);
	pokemon[4]->move = new Moves("Air Slash", types(5), 75);
	pokemon[randomPoke]->cleanTypes();

	return pokemon[randomPoke];

}
//these is the function for initialization of the player
Trainer* startOak() {
	string name;
	int choice;
	Pokemon* pokemon[3];
	pokemon[0] = new Pokemon("Mudkip", 30, 18, 15, 10);
	pokemon[0]->type[0] = types(0);
	pokemon[0]->evolveLvl = 16;
	pokemon[0]->evolve = new Pokemon("Marshtomp");
	pokemon[0]->evolve->type[0] = types(0);
	pokemon[0]->evolve->type[1] = types(8);
	pokemon[0]->evolve->evolveLvl = 36;
	pokemon[0]->evolve->evolve = new Pokemon("Swampert");
	pokemon[0]->evolve->evolve->type[0] = types(0);
	pokemon[0]->evolve->evolve->type[1] = types(8);
	pokemon[0]->move = new Moves("Water gun", types(0), 40);
	pokemon[0]->evolve->move = new Moves("Mudshot", types(8), 55);
	pokemon[0]->evolve->evolve->move = new Moves("Muddy Water", types(0), 90);

	pokemon[1] = new Pokemon("Torchic", 35, 15, 15, 10);
	pokemon[1]->type[0] = types(9);
	pokemon[1]->evolveLvl = 16;
	pokemon[1]->evolve = new Pokemon("Combusken");
	pokemon[1]->evolve->type[0] = types(9);
	pokemon[1]->evolve->type[1] = types(6);
	pokemon[1]->evolve->evolveLvl = 36;
	pokemon[1]->evolve->evolve = new Pokemon("Blaziken");
	pokemon[1]->evolve->evolve->type[0] = types(9);
	pokemon[1]->evolve->evolve->type[1] = types(6);
	pokemon[1]->move = new Moves("Ember", types(9), 40);
	pokemon[1]->evolve->move = new Moves("Flame Charge", types(9), 50);
	pokemon[1]->evolve->evolve->move = new Moves("Blaze Kick", types(6), 85);


	pokemon[2] = new Pokemon("Treecko", 35, 18, 13, 10);
	pokemon[2]->type[0] = types(1);
	pokemon[2]->evolveLvl = 16;
	pokemon[2]->evolve = new Pokemon("Grovyle");
	pokemon[2]->evolve->type[0] = types(1);
	pokemon[2]->evolve->evolveLvl = 36;
	pokemon[2]->evolve->evolve = new Pokemon("Sceptile");
	pokemon[2]->evolve->evolve->type[0] = types(1);
	pokemon[2]->move = new Moves("Razor leaf", types(1), 40);
	pokemon[2]->evolve->move = new Moves("Leaf Blade", types(1), 90);
	pokemon[2]->evolve->evolve->move = new Moves("Leaf Blade", types(1), 90);

	cout << "Welcome to the World of Pokemon.\nWhat is your name? " << endl;
	cin >> name;
	cout << "I currently have 3 pokemon here, choose 1: ";
	for (int i = 0; i < 3; i++) {
		cout << i + 1 << ": " << pokemon[i]->name;
		if (i < 2) cout << ", ";
	}
	cout << endl;
	do cin >> choice; while (choice <= 0 || choice >= 4);

	cout << "you've chosen " << pokemon[choice - 1]->name << " it's now time to start your very own journey " << name << "." << endl;

	pokemon[choice - 1]->cleanTypes();
	Trainer * trainer = new Trainer(name);
	trainer->pokemon.push_back(pokemon[choice - 1]);
	_getch();
	system("CLS");
	return trainer;
}
//pokemon Battles
void pokemonBattle(Trainer* trainer) {
	Pokemon* pokemon = pokemons();
	int start = 0;
	bool isCaptured = false;
	bool escaped = false;

	cout << "wild " << pokemon->name << " have appeared... go " << trainer->pokemon[start]->name <<	endl;
	while (pokemon->hp > 0 && isCaptured == false && trainer->location->currentLoc() != false && escaped == false) {
		int choice;
		trainer->pokemon[0]->showStats();
		pokemon->showStats();
		cout << string(30, '=') << "\n\t[1] battle\n\t[2] Capture\n\t[3] run\n\t[4] Change Pokemon" << endl;
		do cin >> choice; while (choice < 1 || choice > 4);

		if (choice == 1) trainer->pokemon[0]->move->applyDamage(trainer->pokemon[start], pokemon);
		else if (choice == 2) isCaptured = trainer->capture(pokemon);
		else if (choice == 3) escaped = trainer->chickenOut();
		else trainer->switchPokemons();
		if (escaped == false && isCaptured == false && pokemon->hp > 0)pokemon->move->applyDamage(pokemon, trainer->pokemon[start]);

		if (pokemon->hp <= 0 && escaped == false && isCaptured == false) {
			cout << trainer->pokemon[0]->name << " won the battle...\n";
			trainer->pokemon[0]->gainExp();
			trainer->pokemon[0] = trainer->pokemon[0]->evolution();
			trainer->gainPokeDollars((pokemon->lvl * 100) / 2);
		}
		if (trainer->pokemon[0]->hp <= 0) {
			cout << "Pokemon Fainted...\n";
			trainer->switchPokemons();
			trainer->blackedOut();
		}
		
		trainer->blackedOut();
		_getch();
		system("cls");
	}
}

void  MainAction(Trainer* trainer) {
	int choice;
	cout << "PokeDollars: " << trainer->pokeDollars << endl << endl;
	cout << "What would " << trainer->name << " like to do?" << endl
		<< "1. Would you like to move? "; 
	trainer->displayPosition();
	cout << "2. Display stats of Pokemon?" << endl;
	if (trainer->location->currentLoc() == false) {
		cout << "3. Pokemon Center\n4. Pokemart\n";
		do cin >> choice; while (choice <= 0 || choice > 4);
	}
	else do cin >> choice; while (choice <= 0 || choice > 2);
	system("Cls");
	if (choice == 1) {
		int direction;
		cout << "\t[1] North\n\t[2] South\n\t[3] East\n\t[4] West\n";
		do cin >> direction; while (direction > 4 || direction < 1);

		if (direction <= 2) trainer->moveVertical(direction);
		else trainer->moveHorizontal(direction % 2);

		if (trainer->location->appearEnemy() == true) pokemonBattle(trainer);
	}
	else if (choice == 2) trainer->showAllPokemon();
	else if (choice == 3) trainer->goPokemonCenter();
	else trainer->goPokemart();
	
	system("cls");
}

int main() {
	srand(time(NULL));
	Trainer* trainer = startOak();
	trainer->location = Hoenn();

	while (true) {
		MainAction(trainer);
	}
	delete trainer;
	system("pause");
	return 0;
}