#include "Pokemon.h"
#include "Types.h"
#include "Moves.h"

Pokemon::Pokemon(string name, int baseHp, int atk, int def, int lvl)
{
	this->name = name;
	this->baseHp = baseHp;
	this->hp = this->baseHp;
	this->maxHp = this->hp;
	this->atk = atk;
	this->def = def;
	this->lvl = lvl;
	this->maxExp = 20;
}

Pokemon::Pokemon(string name, int baseHp, int atk, int def, int lvl, Types* type1, Types* type2, Moves* move)
{
	this->name = name;
	this->baseHp = baseHp;
	this->hp = this->baseHp;
	this->maxHp = this->hp;
	this->atk = atk;
	this->def = def;
	this->lvl = lvl;
	this->maxExp = 20;
	this->type[0] = type1;
	this->type[1] = type2;
	this->move = move;
}

Pokemon::Pokemon(string name) {
	this->name = name;
}

void Pokemon::cleanTypes() {
	// if the pokemon has two types
	if (sizeof(*this->type) / sizeof(this->type[0]) == 2) {
		//removing weaknesses and resistances in the other type if there is an immunity
		for (int l = 0; l < sizeof(*this->type) / sizeof(this->type[0]); l++) {
			if (this->type[l]->immune.size() != 0) {
				for (int i = 0; i < this->type[l + 1 % 2]->immune.size(); i++) {
					for (int j = 0; j < this->type[l + i % 2]->strength.size(); j++) {
						if (this->type[l + 1 % 2]->strength[j] == this->type[0]->immune[i])
							this->type[1 + 1 % 2]->strength.erase(this->type[1]->strength.begin() + j);
					}

					for (int j = 0; j < this->type[l + 1 % 2]->weaknesses.size(); j++) {
						if (this->type[l + 1 % 2]->weaknesses[j] == this->type[0]->immune[i])
							this->type[l + 1 % 2]->weaknesses.erase(this->type[1]->weaknesses.begin() + j);
					}
				}
			}
		}
		
		// removing the contradicting weakness and resistances of both types
		for (int k = 0; k < sizeof(*this->type) / sizeof(this->type[0]); k++) {
			for (int i = 0; i < this->type[k]->strength.size(); i++) {
				for (int j = 0; j < this->type[k + 1 % 2]->weaknesses.size(); j++) {
					if (this->type[k + 1 % 2]->weaknesses[j] == this->type[k]->strength[i]) {
						this->type[k + 1 % 2]->weaknesses.erase(this->type[1]->weaknesses.begin() + j);
						this->type[k]->strength.erase(this->type[0]->strength.begin() + i);
					}
				}
			}
		}
	}
}

Pokemon* Pokemon::evolution()
{
	if (this->evolveLvl <= this->lvl && this->evolveLvl != NULL) {
		Pokemon* pokemon = this;
		cout << "oho.... " << this->name << "is evolving...";
		_getch();
		cout << this->name << " evolved to " << this->evolve->name << endl;
		pokemon->evolve->hp = this->hp;
		pokemon->evolve->maxHp = this->maxHp + 10;
		pokemon->evolve->atk = this->atk + 3;
		pokemon->evolve->def = this->def + 3;
		pokemon->evolve->lvl = this->lvl;
		pokemon->evolve->exp = this->exp;
		pokemon->evolve->maxExp = this->maxExp;
		pokemon = this->evolve;
		pokemon->cleanTypes();
		return pokemon;
	}
}

void Pokemon::lvlUp() {
	/*HP grows 15% per level
Damage grows 10 % per level
Exp to Next Level grows 20% per level
Exp to Give grows 20% per level*/
	cout << this->name << " leveled up to " << this->lvl++;
	this->maxHp += this->baseHp * 0.15f;
	this->atk += this->atk * 0.1f;
	this->exp %= this->maxExp;
	this->maxExp += this->maxExp * .2f;
	this->lvl++;
	if (this->exp >= this->maxExp) this->lvlUp();
}

void Pokemon::gainExp() {												
	cout << this->name << " gained " << this->maxExp * .2f << " exp.\n";
	this->exp += this->maxExp * 0.2f;
	if (this->exp >= this->maxExp) this->lvlUp();
}

void Pokemon::showStats(){
	cout << "Pokemon : " << this->name << "\tlevel: " << this->lvl << "\texp: " << this->exp << " / " << this->maxExp << endl;
	for (int i = 0; i < sizeof(*this->type) / sizeof(this->type[0]); i++) cout << "Type " << i + 1 << ": " << this->type[i]->name << endl;
	cout << "Hp : " << this->hp << " / " << this->maxHp << endl
		<< "Atk: " << this->atk << endl
		<< "Def: " << this->def << endl << endl;
}

void Pokemon::showWeaknesses() {
	cout << "weaknesses: ";
	for (int i = 0; i < 2; i++) {
		cout << "Type 1: ";
		for (int j = 0; j < this->type[i]->immune.size(); j++) {
			cout << this->type[i]->immune[j];
			if (j < this->type[i]->immune.size() - 1) cout << ", ";
		}
		cout << endl;
	}
}
