#pragma once
#include <iostream>
#include <string>
#include<vector>;

using namespace std;
class Pokemon;
class Moves;
class Types
{
public:
	Types();
	Types(string name, vector<string> strength, vector<string> weaknesses);
	Types(string name, vector<string> immunities, vector<string> strength, vector<string> weaknesses);
	string name;
	vector<string> strength;
	vector<string> weaknesses;
	vector<string> immune;
	

};

