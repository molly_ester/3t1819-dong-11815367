#include "Map.h"
#include "Pokemon.h"

Map::Map(int xL, int xR, int yU, int yD, bool isWild, string name)
{
	this->xL = xL;
	this->xR = xR;
	this->yU = yU;
	this->yD = yD;
	this->isWild = isWild;
	this->name = name;
}

bool Map::appearEnemy() {
	if (rand() % 100 + 1 <= 50 && isWild != false) {
		system("Cls");
		return true; //meaning player has encountered pokemon;
	}
	else return false;
}

bool Map::currentLoc() {
	return this->isWild;
}


