#pragma once
#include <iostream>
#include <string>
#include<vector>

using namespace std;
class Pokemon;
class Map;
class Trainer
{
private: int x, y;
public:
	Trainer();
	Trainer(string name);
	string name;
	vector<Pokemon*> pokemon;
	Map* location;
	int pokeballs[3];
	int pokeDollars;

	void moveHorizontal(int x);
	void moveVertical(int y);
	void moveHorizontalLocation();
	void moveVerticalLocation();
	bool capture(Pokemon* pokemon);
	void buyItems(int price, int choice, int howMany);
	void switchPokemons();
	void blackedOut();
	bool chickenOut(); 
	void goPokemart();
	void goPokemonCenter();
	void showAllPokemon();
	void displayPosition();
	void gainPokeDollars(int money);
};

